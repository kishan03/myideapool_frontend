# MyIdeaPool Front End

This is ReactJS frontend providing UI for MyIdea Pool app.

## Steps to run front end locally.

1. Clone the repository using `git clone https://kishan03@bitbucket.org/kishan03/myideapool_frontend.git`
2. Install dependencies using `yarn install`
3. Start the ReactJS server `yarn start`
4. If all went fine, a new browser window should open at `localhost:3000`

[Live demo](https://laughing-lalande-5a7694.netlify.com/)