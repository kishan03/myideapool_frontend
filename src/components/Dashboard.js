import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import IdeaPool_icon from '../images/IdeaPool_icon.png'
import AddCircle from '@material-ui/icons/AddCircle'
import * as Api from '../utils/api'
import { Redirect } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Avatar from '@material-ui/core/Avatar'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Idea from './Idea'


class Dashboard extends Component {
	state = {
		ideas: [],
		access_token: "",
		name: "",
		avatar: "",
		redirectToLogin: false,
		open: false,
		content: "",
		impact: 1,
		ease: 1,
		confidence: 1,
	}

	componentDidMount() {
		const access_token = localStorage.getItem("access_token")
		Api.getIdeas(access_token)
		.then((ideas) => {
			Api.getUserDetails(access_token)
			.then((userdetails) => {
				if (ideas.code === "token_not_valid") {
					this.setState({
						redirectToLogin: true,
					})	
				}
				else {
					this.setState({
						name : userdetails.name,
						avatar : userdetails.avatar_url,
						ideas,
						access_token,
					})
				}	
			})
			
		})	
	}
	
	handleInputChange = (e) => {
		const text = e.target.value
		const name = e.target.name
        if (name === 'content') {
            this.setState(() => ({
                content: text
            }))
        }
        else if (name === 'impact') {
        	this.setState(() => ({
                impact: text
            }))
        }
        else if (name === 'ease') {
        	this.setState(() => ({
                ease: text
            }))
        }
        else {
            this.setState(() => ({
                confidence: text
            }))
        }
	}

  	handleClickOpen = () => {
    	this.setState({ open: true });
  	}

  	handleClose = () => {
    	this.setState({ open: false });
  	}
	
	handleLogout = (e) => {
		e.preventDefault()
		localStorage.clear()
		this.setState({
			redirectToLogin: true,
		})
	}
	
	handleDeleteIdea = (id) => {
		const { ideas, access_token } = this.state
		Api.deleteIdea(access_token, id)
		.then(() => {
			this.setState({
				ideas: ideas.filter((idea) => idea.id !== id)
			})
		})
	}
	
	handleEditIdea = (idea, id) => {
		const { access_token } = this.state
		Api.updateIdea(access_token, idea, id)
	}

	handleAddIdea = (e) => {
		e.preventDefault()
		const { access_token, content, impact, ease, confidence } = this.state
		Api.createIdea(access_token, content, impact, ease, confidence)
		.then((idea) => {
			if (idea.code === undefined) {
				this.setState((currState) => ({
					ideas: currState.ideas.concat(idea),
				}))
				this.handleClose()
			}
		})
	}

	render() {
		const { redirectToLogin, ideas, content, ease, impact, confidence, avatar, name } = this.state
		if (redirectToLogin) {
			return <Redirect to="/" />
		}
		return (
			<div>
                <Grid container spacing={0} style={{ padding: 20 }}>
                    <Grid xs={1}>
                        <img src={IdeaPool_icon} alt="idealpool_icon" />
                    </Grid>
                    <Grid xs={8}>
                    <h1>My Idea Pool</h1>
                    </Grid>
                    <Grid xs={1}>
                        <Avatar alt="Adelle Charles" src={avatar} />
                    </Grid>
                    <Grid> 
                        <h4>Hello {name}!</h4>
                    </Grid>
                    <Grid xs={1}>
                        <Button variant="contained" onClick={this.handleLogout}>LOGOUT</Button>
                    </Grid>
                    
                    <Grid item sm={8}>
                        {ideas.map((idea) => (
                            <Idea key={idea.id} idea={idea} handleEditIdea={this.handleEditIdea} handleDeleteIdea={this.handleDeleteIdea} />
                        ))}
                    </Grid>
                    <Grid item sm={4}>
                        <div>
                            <Button aria-label="Add" onClick={this.handleClickOpen}>
                                <AddCircle style={{ fontSize: 60, color: 'white' }}/>
                                {/* <img alt="Add Idea" src={AddCircle} /> */}
                            </Button>
                            <Dialog
                            open={this.state.open}
                            onClose={this.handleClose}
                            aria-labelledby="form-dialog-title"
                            >
                            <DialogTitle id="form-dialog-title">Add Idea</DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                To create idea please put all four values.
                                </DialogContentText>
                                <TextField
                                margin="dense"
                                name="content"
                                onChange={this.handleInputChange}
                                value={content}
                                label="Content"
                                />
                                <TextField
                                margin="dense"
                                name="impact"
                                onChange={this.handleInputChange}
                                value={impact}
                                inputProps={{ max: 10 }}
                                type="number"
                                label="Impact"
                                />
                                <TextField
                                margin="dense"
                                name="ease"
                                onChange={this.handleInputChange}
                                value={ease}
                                inputProps={{ max: 10 }}
                                type="number"
                                label="Ease"
                                />
                                <TextField
                                margin="dense"
                                type="number"
                                onChange={this.handleInputChange}
                                value={confidence}
                                inputProps={{ max: 10 }}
                                name="confidence"
                                label="Confidence"
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleClose} color="secondary">
                                Cancel
                                </Button>
                                <Button onClick={this.handleAddIdea} color="primary">
                                Add Idea
                                </Button>
                            </DialogActions>
                            </Dialog>
                        </div>
                    </Grid>
                </Grid> 
            </div>
		)
	}
}

export default Dashboard