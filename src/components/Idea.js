import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField'
import Update from '@material-ui/icons/Update'
import Delete  from '@material-ui/icons/Delete'
import Grid from '@material-ui/core/Grid'


class Idea extends Component {
	state = {
		content: "",
		impact: 0,
		ease: 0,
		confidence: 0,
	}
	
	componentDidMount() {
		const { idea } = this.props
		this.setState({
			content: idea.content,
			impact: idea.impact,
			ease: idea.ease,
			confidence: idea.confidence,
		})
	}
	
	
	handleInputChange = (e) => {
		const text = e.target.value
		const name = e.target.name
        if (name === 'content') {
            this.setState(() => ({
                content: text
            }))
        }
        else if (name === 'impact') {
        	this.setState(() => ({
                impact: text
            }))
        }
        else if (name === 'ease') {
        	this.setState(() => ({
                ease: text
            }))
        }
        else {
            this.setState(() => ({
                confidence: text
            }))
        }
	}

	handleDelete = (access_token, id) => {
		this.props.handleDeleteIdea(id)
	}
	
	handleEdit = (content, ease, impact, confidence, id) => {
		const idea = {"content": content,
		"ease": ease,
		"impact": impact,
		"confidence": confidence
		}

		this.props.handleEditIdea(idea, id)
	}

	render() {
		const { idea } = this.props
		const { content, ease, impact, confidence } = this.state
		const access_token = localStorage.getItem("access_token")
		return (
			<Grid container spacing={24}>
				<Grid item sm={4}>
					<TextField type='text' onChange={this.handleInputChange} name="content" value={content} />
				</Grid>
				<Grid item sm={2}>
					<TextField type='number' value={ease} />
				</Grid>
				<Grid item sm={2}>
					<TextField type='number' value={impact} />
				</Grid>
				<Grid item sm={2}>
					<TextField type='number' className={"text"} value={confidence} />
				</Grid>
				<Grid item sm={1}>
					<div onClick={() => this.handleEdit(content, ease, impact, confidence, idea.id)}><Update style={{ fontSize: 40, color: 'white' }}/></div>
				</Grid>
				<Grid item sm={1}>
					<div onClick={() => this.handleDelete(access_token, idea.id)}><Delete style={{ fontSize: 40, color: 'white' }} /></div>
				</Grid>
			</Grid>
		)
	}
}

export default Idea